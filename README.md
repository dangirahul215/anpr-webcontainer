# README #

This README documents the steps that are necessary to get the anpr application up and running.

## How do I get set up? ##

### Manual Setup without Docker

->There must be node.js and python installed in the system.

#### To setup and start the Flask back-end 

-> In the ./Server folder an virtual environment is available
-> Open terminal to install virtual env. 
**Enter > pip install virtualenv**

-> Once virtualenv is installed in the same folder activate virtualenv. 
**Enter > ./env/Scripts/activate.ps1** ( Windows)
or    **> ./env/Scripts/activate (Ubuntu)**

-> If this throws up an execution policy error, open terminal as administrator
**Enter > Set-ExecutionPolicy unrestricted**
**Enter > Yes**
**Enter > exit**
and go back to the Server folder and **Enter > ./env/Script/activate.ps1** 

-> Now our virtualenv is activated, virtual environment helps us to install all the necessary dependencies locally,
which means our System doesn't get effected if at all it already has a pre-existing version of these dependencies

-> To install dependencies, Enter the following in the terminal where virtualenv is active 
**Enter> pip install requirements.txt**

->Once the requirements are installed
**Enter > python ./app.py**

->Now our python app is running, by default on porrt 5000.
If the above commands throws an error, check if app.config[SQLALCHEMY_DATABASE_URI] is correct or not. 
We need to provide a uri where the database is active.

#### To setup and start the React back-end

-> Now, go into the ../Client/number-plate-recognition/ folder 

->Make sure you have npm installed. (it generally gets installed along with node.js)

-> Package.json has the information about all the modules that will be required for the app to run.

-> In the terminal,
**Enter > npm install**

-> This will install all the necessary modules and dependencies

->To start the React front-end,
**Enter > npm start**

-> The react app will start on the local host at port 3000 (by default).
